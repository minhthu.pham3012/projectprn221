﻿
using Domain.Base;
using Domain.Entities;
using System;

namespace Domain.EntityRelationship
{
    public class ProductProductDetail : BaseEntity
    {
        public Guid ProductId;
        public Guid ProductDetailId;
        public Product Product { get; set; }
        public ProductDetail ProductDetail { get; set; }
    }
}
