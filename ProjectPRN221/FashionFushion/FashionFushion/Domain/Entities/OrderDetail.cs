﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class OrderDetail: BaseEntity
    {
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public ICollection<ProductDetail> ProductDetails { get; set; }
        public Guid OrderId { get; set; }   
        public Order Order { get; set; }
    }
}
