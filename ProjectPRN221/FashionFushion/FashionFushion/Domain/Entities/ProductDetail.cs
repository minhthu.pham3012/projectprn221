﻿using Domain.Base;
using Domain.EntityRelationship;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ProductDetail: BaseEntity
    {
        public int Quantity { get; set; }
        public string Size { get; set; }
        public ICollection<ProductProductDetail> ProductProductDetails { get; set; }
        public ICollection<CartItem> CartItems { get; set; }
        public Guid OrderDetailId { get; set; } 
        public OrderDetail OrderDetail { get; set; }
    }
}
