﻿using Domain.Base;
using Domain.Enum.RoleEnum;
using Domain.Enum.StatusEnum;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class User : BaseEntity
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public bool Gender { get; set; }
        public Roles Roles { get; set; }
        public Status Status { get; set; } 
        public Cart Cart { get; set; }
        public ICollection<Product> Products { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
