﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Cart: BaseEntity
    {
        public decimal Total { get; set; }
        public ICollection<CartItem> CartItems { get; set; } 
        public User User { get; set; }
    }

}
