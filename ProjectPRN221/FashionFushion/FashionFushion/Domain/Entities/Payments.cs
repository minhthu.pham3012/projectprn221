﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Payments : BaseEntity
    {
        public decimal Total { get; set; }  
        public DateTime DateOrder { get; set; }
    }
}
