﻿using Domain.Base;
using Domain.EntityRelationship;
using Domain.Enum.StatusEnum;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Product : BaseEntity
    {
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public decimal Price { get; set; }
        public Status Status { get; set; }
        public Guid UserId { get; set; }    
        public User User { get; set; }
        public ICollection<ProductProductDetail> ProductProductDetails { get; set; }
        public Guid Categoryid { get; set; }    
        public Category category { get; set; }
    }
}
