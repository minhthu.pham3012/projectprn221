﻿using Applications.Repositories;
using System.Threading.Tasks;

namespace Applications
{
    public interface IUnitOfWork
    {
        public IUserRepository UserRepository { get; }

        public Task<int> SaveChangeAsync();
    }
}
