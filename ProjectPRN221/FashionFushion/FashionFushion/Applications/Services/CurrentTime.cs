﻿using Applications.Interfaces;
using System;

namespace Applications.Services
{
    public class CurrentTime : ICurrentTime
    {
        DateTime ICurrentTime.CurrentTime() => DateTime.UtcNow;
    }
}
