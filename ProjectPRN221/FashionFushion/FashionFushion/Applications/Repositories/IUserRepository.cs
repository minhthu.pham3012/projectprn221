﻿using Applications.Commons;
using Domain.Entities;
using System.Threading.Tasks;
using Domain.Enum.RoleEnum;

namespace Applications.Repositories
{
    public interface IUserRepository
    {
        Task<User?> GetUserByEmail(string email);
        Task<Pagination<User>> GetUsersByRole(Roles role, int pageNumber = 0, int pageSize = 10);
        Task<Pagination<User>> SearchUserByName(string name, int pageNumber = 0, int pageSize = 10);
        //Task<Pagination<User>> FilterUser(FilterUserRequest filterUserRequest, int pageNumber = 0, int pageSize = 10);
    }
}
