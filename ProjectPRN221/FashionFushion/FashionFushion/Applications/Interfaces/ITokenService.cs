﻿using Applications.ViewModels.TokenViewModels;
using System.Threading.Tasks;

namespace Applications.Interfaces
{
    public interface ITokenService
    {
        Task<TokenModel> GetToken(string email);
    }
}
