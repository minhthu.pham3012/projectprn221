﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Domain.Enum.RoleEnum;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly AppDBContext _appDBContext;

        public UserRepository(AppDBContext appDBContext, ICurrentTime currentTime, IClaimService claimService) : base(appDBContext, currentTime, claimService)
        {
            _appDBContext = appDBContext;
        }

        public Task<User> GetUserByEmail(string email)
        {
            throw new System.NotImplementedException();
        }

        public Task<Pagination<User>> GetUsersByRole(Roles role, int pageNumber = 0, int pageSize = 10)
        {
            throw new System.NotImplementedException();
        }

        public Task<Pagination<User>> SearchUserByName(string name, int pageNumber = 0, int pageSize = 10)
        {
            throw new System.NotImplementedException();
        }
    }
}
