﻿
using Domain.Entities;
using Domain.EntityRelationship;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class ProductProductDetailConfig : IEntityTypeConfiguration<ProductProductDetail>
    {
        public void Configure(EntityTypeBuilder<ProductProductDetail> builder)
        {
            builder.HasKey(k => new {k.ProductId, k.ProductDetailId});

            //builder.Ignore(i => i.Id);

            builder.HasOne<Product>(p => p.Product).WithMany(pr => pr.ProductProductDetails).HasForeignKey(p => p.ProductId);

            builder.HasOne<ProductDetail>(p => p.ProductDetail).WithMany(pr => pr.ProductProductDetails).HasForeignKey(p => p.ProductDetail);
        }
    }
}
