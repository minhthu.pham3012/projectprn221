﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class ProductDetailConfig: IEntityTypeConfiguration<ProductDetail>
    {
        public void Configure(EntityTypeBuilder<ProductDetail> builder)
        {
            builder.HasOne(x => x.OrderDetail)
                  .WithMany(x => x.ProductDetails)
                  .HasForeignKey(x => x.OrderDetailId);

        }
    }
}
