﻿using Domain.Entities;
using Domain.EntityRelationship;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Infrastructures
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<ProductDetail> ProductDetails { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Payments> Payments { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
        public DbSet<ProductProductDetail> ProductProductDetails { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Product>().Property(x => x.Price).HasColumnType("money");
            modelBuilder.Entity<Payments>().Property(x => x.Total).HasColumnType("money");
            modelBuilder.Entity<OrderDetail>().Property(x => x.Price).HasColumnType("money");
            modelBuilder.Entity<Cart>().Property(x => x.Total).HasColumnType("money");
            modelBuilder.Entity<User>()
                .HasOne(p => p.Cart)
                .WithOne(p => p.User)
                .HasForeignKey<Cart>(p => p.Id);
        }
    
    }
}
