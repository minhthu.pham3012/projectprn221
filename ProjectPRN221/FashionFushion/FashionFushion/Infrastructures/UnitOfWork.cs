﻿using Applications;
using Applications.Repositories;
using System.Threading.Tasks;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDBContext _appDBContext;
        private readonly IUserRepository _userRepository;

        public UnitOfWork(AppDBContext appDBContext, IUserRepository userRepository)
        {
            _appDBContext = appDBContext;
            _userRepository = userRepository;
        }

        public IUserRepository UserRepository => _userRepository;

        public async Task<int> SaveChangeAsync() => await _appDBContext.SaveChangesAsync();
    }
}
