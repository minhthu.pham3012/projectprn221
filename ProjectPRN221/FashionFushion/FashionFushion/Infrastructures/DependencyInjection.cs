﻿using Applications;
using Applications.Interfaces;
using Applications.Repositories;
using Applications.Services;
using Infrastructures.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Infrastructures
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            //services.AddScoped<ICurrentTime, CurrentTime>();

            //Connection Sql
            services.AddDbContext<AppDBContext>(options => options.UseSqlServer(configuration.GetConnectionString("AppDB")));

            //Add object
            services.AddScoped<IUserRepository, UserRepository>();
            return services;
        }
    }
}
